﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treasure.EntityModell;

namespace Treasure.DBaseOperations
{
    public class DBMedicins
    {
        public static async void MedicineExists(EntityModell.Medicine medicine, Action<bool,string> callback = null)
        {
            IEnumerable<Medicine> pon = null;
            try
            {
                string estr = medicine.nev;
                var splitted = medicine.nev.Split('_');

                if (!string.IsNullOrEmpty(medicine.nev) && int.TryParse(splitted.Last(), out int num))
                {
                    for (int i = 0; i < splitted.Length - 1; i++)
                    {
                        estr = string.Join("", estr, splitted[i]);
                    }
                }

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@nev", $"%{estr}%");
                    dnm.Add("@adagolas", medicine.adagolas);

                    pon = await connection.QueryAsync<Medicine>("select * from `gyogyszer` where `nev` like @nev ", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (pon != null && pon.Count() > 0)
            {
                var NameNew = string.Join("", medicine.nev, $"_{pon.Count()}");
                callback(pon != null && pon.Count() > 0, NameNew);
            }
            else
            {
                callback(pon != null && pon.Count() > 0, medicine.nev);
            }

            
        }
        //INSERT INTO `gyogyszer`( `nev`, `reszletek`, `adagolas`, `kep`) VALUES ('as','as','as','as')
        public static async void AddMedicine(EntityModell.Medicine medicine, Action<bool> callback = null)
        {
            int pon = 0;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@nev", medicine.nev);
                    dnm.Add("@reszletek", medicine.reszletek);
                    dnm.Add("@adagolas", medicine.adagolas);
                    dnm.Add("@kep", medicine.kep);

                    pon = await connection.ExecuteAsync("INSERT IGNORE INTO `gyogyszer`( `nev`, `reszletek`, `adagolas`, `kep`) VALUES (@nev,@reszletek,@adagolas,@kep)", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon == 1);
        }
         
        public static async void GetMedicineIDAssociatedByItsName(EntityModell.Medicine medicine, Action<int> callback = null)
        {
            IEnumerable< int> pon = null;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@nev", medicine.nev);


                    pon = await connection.QueryAsync<int>("SELECT `ID` FROM `gyogyszer` WHERE `nev` = @nev", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (pon.Count() == 1) callback(pon.First());
            else callback(-1);
        }
        public static async void AddMedicineToUser(BoughtMedicine boughtMedicine,Action<bool> callback)
        {
            int pon = 0;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@user_id", boughtMedicine.user_id);
                    dnm.Add("@gyogyszer_id", boughtMedicine.gyogyszer_id);
                    dnm.Add("@mennyiseg", boughtMedicine.mennyiseg);
                    dnm.Add("@lejarat", boughtMedicine.lejarat);

                    pon = await connection.ExecuteAsync("INSERT INTO `megvasaroltgyogyszer`( `user_id`, `gyogyszer_id`, `mennyiseg`, `lejarat`) VALUES (@user_id,@gyogyszer_id,@mennyiseg,@lejarat)", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon == 1);
        }
        // Update BoughtMedicine
        //UPDATE `megvasaroltgyogyszer` SET `mennyiseg` = '52', `lejarat` = '2018-11-29 00:00:00' WHERE `megvasaroltgyogyszer`.`ID` = 1
        public static async void UpdateUsersMedicineDetails(BoughtMedicine boughtMedicine, Action<bool> callback)
        {
            int pon = 0;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@ID", boughtMedicine.ID);
                    dnm.Add("@user_id", boughtMedicine.user_id);
                    dnm.Add("@gyogyszer_id", boughtMedicine.gyogyszer_id);
                    dnm.Add("@mennyiseg", boughtMedicine.mennyiseg);
                    dnm.Add("@lejarat", boughtMedicine.lejarat);

                    pon = await connection.ExecuteAsync("UPDATE `megvasaroltgyogyszer` SET `mennyiseg` = @mennyiseg, `lejarat` = @lejarat WHERE `megvasaroltgyogyszer`.`ID` = @ID", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon == 1);
        }
        public static async void GetUsersMedicines(int UsrId, Action<List<UsersMedicine>> callback = null)
        {
            IEnumerable<UsersMedicine> pon = null;
            
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@UID", UsrId);


                    pon = await connection.QueryAsync<UsersMedicine>("select `meg`.`ID`,`nev`,`reszletek`,`adagolas`,`kep`,`mennyiseg`, `lejarat` from `gyogyszer` as `gyo` inner JOIN `megvasaroltgyogyszer` as `meg` on `gyo`.`ID` = `meg`.`gyogyszer_id` where `gyo`.`ID` in (SELECT `gyogyszer_id` FROM `megvasaroltgyogyszer` WHERE `user_id` = @UID) and `user_id` = @UID", dnm);
                    

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon.ToList());
            
        }
        public static async void DeleteUsersMedicine(int ID, Action<bool> callback = null)
        {
            int pon = 0;
            try
            {



                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    //DELETE FROM `gyogyszer` WHERE `gyogyszer`.`ID` = 86
                    // Megkeressük a gyógyszer ID -ját

                    var dnm = new DynamicParameters();
                    dnm.Add("@ID", ID);

                    var medicine = connection.QueryFirst<Medicine>("select * from `gyogyszer` where `ID` = (select `gyogyszer_id` from `megvasaroltgyogyszer` where `ID` = @ID)", dnm);


                    pon = await connection.ExecuteAsync("DELETE FROM `megvasaroltgyogyszer` WHERE `ID` = @ID", dnm);

                    // Ha van a gyógyszer nevének a végén egy szám miután '_' karakterre spliteltem, akkor törölni kell a többszöröződés elkerülése érdekében.
                    if (!string.IsNullOrEmpty(medicine.nev) && int.TryParse(medicine.nev.Split('_').Last(),out int num) )
                    {
                        dnm.Add("@MedicineID",medicine.ID);
                        pon = await connection.ExecuteAsync("DELETE FROM `gyogyszer` WHERE `ID` = @MedicineID", dnm);
                    }

                    

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon == 1);
        }
        public static async void GetAllMedicine( Action<List<UsersMedicine>> callback = null)
        {
            IEnumerable<UsersMedicine> pon = null;

            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {



                    //pon = await connection.QueryAsync<UsersMedicine>("select `meg`.`ID`,`nev`,`reszletek`,`adagolas`,`kep`,`mennyiseg`, `lejarat` from `gyogyszer` as `gyo` inner JOIN `megvasaroltgyogyszer` as `meg` on `gyo`.`ID` = `meg`.`gyogyszer_id` where `gyo`.`ID` in (SELECT `gyogyszer_id` FROM `megvasaroltgyogyszer` )");
                    pon = await connection.QueryAsync<UsersMedicine>("SELECT * FROM `gyogyszer` group by `kep`");


                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon.ToList());

        }
        public static async void GetIDFromUserAndMedicine(int uID,int mID, Action<int> callback = null)
        {
            IEnumerable<int> pon = null;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@uID", uID);
                    dnm.Add("@mID", mID);
                    pon = await connection.QueryAsync<int>("SELECT ID from `megvasaroltgyogyszer` where `user_id` = @uID and `gyogyszer_id` = @mID", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon.Count()==1?pon.First():-1 );
        }
    }
}
