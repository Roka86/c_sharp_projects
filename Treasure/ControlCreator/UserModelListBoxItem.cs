﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Treasure.ControlCreator
{
    public class UserModelListBoxItem : ListBoxItem
    {
        public EntityModell.User User { get; }

        public TextBlock UserName;
        private Image LockedImage;
        private bool _IsLoggedIn;
         

        public bool IsLoggedIn
        {
            get { return _IsLoggedIn; }
            set
            {
                _IsLoggedIn = value;

                BitmapImage bj = new BitmapImage();
                bj.BeginInit();
                bj.UriSource = value ? new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\LockedIn.png"))
                                : new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\LockedOut.png"));
                bj.EndInit();
                LockedImage.Source = bj;
                UpdateLayout();
            }
        }


        public UserModelListBoxItem(EntityModell.User user, EntityModell.User currUser, ListBoxItem boxItem, TextBox[] editorSection)
        {

            User = user;
            //Content = user.Name;
            UserName = new TextBlock() {
                Width= 700, Height=60,
                HorizontalAlignment = HorizontalAlignment.Left,
                FontSize = 36.0,
                FontFamily = new FontFamily("Copper Black"),
                FontWeight = FontWeights.Bold,
                Background = new SolidColorBrush(Color.FromArgb(0xad, 0x77, 0x63, 0x57)),
                //TextWrapping =  TextWrapping.Wrap,
                
                Text = user.Name
            };


            LockedImage = new Image() { Width = 60, Height = 60, HorizontalAlignment = HorizontalAlignment.Center };

            IsLoggedIn = false;

            var stk = new StackPanel()
            {
                Width = 800, Height = 60, Orientation = Orientation.Horizontal
            };
            stk.Children.Add(new Border() { Width = 175, Height = 60, Background = new SolidColorBrush(Color.FromArgb(0x4a, 0x89, 0x8a, 0x8a)) });//8E8B8B
            stk.Children.Add(LockedImage);
            stk.Children.Add(new Border()
            {
                Width = 700,
                Height = 60,
                Child = UserName,
                Padding = new Thickness(15.0, 0.0, 0.0, 0.0),
                HorizontalAlignment = HorizontalAlignment.Left
                //BorderThickness= new Thickness(2.0, 2.0, 2.0, 2.0),
                //BorderBrush = new SolidColorBrush(Color.FromArgb(0xad, 0x4f, 0x60, 0x48))//4F604B
            });
            
            Content = stk;

            this.Selected += (o, e) =>
            {
                
                currUser = User;
                boxItem.Content = string.Concat(User.Name, "'s Medicines");
                editorSection[0].Text = User.Name;
                string psw = "***************";
                editorSection[1].Text = psw;
                editorSection[1].GotFocus += (oo, ee) => 
                {
                    editorSection[1].SelectAll();
                };
                editorSection[1].Focus();
            };

        }

    }
}
