﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Treasure.EntityModell;

namespace Treasure.ControlCreator
{
    public class SimpleMedicineRow : ListBoxItem
    {
        public TextBlock neveGyogyult;
        public Medicine med;

        public SimpleMedicineRow(Medicine medicine)
        {
            med = medicine;
            Height = 155;Width = 900;

            var FinalContainer = new StackPanel()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
                Height = 151,
                Width = 900,
                Orientation = Orientation.Horizontal
            };

            var container = new StackPanel()
            {
                Height = 149, Width = 395,
                Orientation = Orientation.Vertical
            };
            neveGyogyult = new TextBlock()
            {
                FontSize = 16,
                Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xad, 0xaa, 0xaa, 0xaa)),//afafaf
                Height = 24,
                Width = 394,
                Text = medicine.nev,
                //HorizontalScrollBarVisibility = ScrollBarVisibility.Auto
            };
            container.Children.Add(neveGyogyult);
            container.Children.Add(new TextBlock() { Height = 24, Width = 394, Text = medicine.adagolas , FontSize = 16});
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(medicine.kep, UriKind.Absolute);
            bi.EndInit();
            var ProductImage = new System.Windows.Controls.Image()
            {
                Width = 350,
                Height = 100,
                Source = bi
            };
            container.Children.Add(ProductImage);
            FinalContainer.Children.Add(container);
            FinalContainer.Children.Add(
                new TextBox()
                {
                    Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xad, 0xaf, 0xaf, 0xaf)),//afafaf
                    Width = 488,Height=140,Text=medicine.reszletek,
                    TextAlignment = System.Windows.TextAlignment.Center,
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                    TextWrapping = System.Windows.TextWrapping.Wrap,
                    HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                    VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                    FontSize = 16
                }
                );
            Content = FinalContainer;
        }
    }
}
