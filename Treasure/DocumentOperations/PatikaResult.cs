﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.DocumentOperations
{
    public class PatikaResult
    {
        public int CurrUserId { get; set; }
        public int ID { get; set; }
        public string nev { get; set; }
        public string reszletek { get; set; }
        public string adagolas { get; set; }
        public string image { get; set; }
        public int mennyiseg { get; set; }
        public DateTime lejarat { get; set; }
        //public byte[] MediumBlob { get; set; }
        public int user_id { get; set; }
        public int gyogyszer_id { get; set; }
    }
}
