﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Treasure.ControlCreator
{
    class CreateMedicinesQuickSearchWindow
    {
        public CreateMedicinesQuickSearchWindow(TextBox TxtBox, List<string> medicines,Point pnt,MainWindow inst)
        {

            var WindowNW = new DescriptionWindow("",QuickSearch: medicines,point:new MyPoint(pnt.X,pnt.Y+86.0));
            //WindowNW.Left = pnt.X-200.0;
            //WindowNW.Top = pnt.Y+200.0;
            WindowNW.Show();
            //TxtBox.Focus();
            TxtBox.KeyDown += (o, e) => WindowNW.Close();
            WindowNW.Closing += (o, e) =>
            {
                if (Properties.Settings.Default.QuickSearchSelected == "True")
                {
                    TxtBox.Text = Properties.Settings.Default.QuickSearchString;
                    inst.BrowseEnter();
                }
            };
            inst.Closing += (o, e) => WindowNW.Close();
            //TxtBox.LostFocus += (o, e) => WindowNW.Close();
        }
    }
}
