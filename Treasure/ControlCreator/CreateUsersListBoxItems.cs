﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Treasure.ControlCreator
{
    public class CreateUsersListBoxItems 
    {
        ObservableCollection<UserModelListBoxItem> items;
        MainWindow Inst;

        public CreateUsersListBoxItems(MainWindow instance)
        {
            Inst = instance;
            items = new ObservableCollection<UserModelListBoxItem>();
            DBaseOperations.DBUsers.GetAllUser(users => GetItems(users));
        }
        public void GetItems(ObservableCollection<EntityModell.User> users)
        {
            Inst.UsersGridTable.Items.Clear();
            foreach (var item in users)
            {
                Inst.UsersGridTable.Items.Add(new UserModelListBoxItem(item, Inst.CurrentSelectedUser, Inst.UsersMedicinesButton, new TextBox[]{Inst.TBCurrUser, Inst.TBCurrPassw}));
            }
            Inst.UpdateLayout();
        }
    }
}
