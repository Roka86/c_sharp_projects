-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Nov 19. 02:41
-- Kiszolgáló verziója: 10.1.36-MariaDB
-- PHP verzió: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `pharmacy`
--


-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyogyszer`
--

CREATE TABLE `gyogyszer` (
  `ID` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `reszletek` varchar(7999) COLLATE utf8_hungarian_ci NOT NULL,
  `adagolas` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kep` varchar(3000) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megvasaroltgyogyszer`
--

CREATE TABLE `megvasaroltgyogyszer` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gyogyszer_id` int(11) NOT NULL,
  `mennyiseg` int(11) NOT NULL,
  `lejarat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `Name` varchar(150) COLLATE utf8_hungarian_ci NOT NULL,
  `Password` varchar(150) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;



--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `gyogyszer`
--
ALTER TABLE `gyogyszer`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `nev` (`nev`);

--
-- A tábla indexei `megvasaroltgyogyszer`
--
ALTER TABLE `megvasaroltgyogyszer`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `gyogyszer_id` (`gyogyszer_id`),
  ADD KEY `user_id` (`user_id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Name` (`Name`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `gyogyszer`
--
ALTER TABLE `gyogyszer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT a táblához `megvasaroltgyogyszer`
--
ALTER TABLE `megvasaroltgyogyszer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `megvasaroltgyogyszer`
--
ALTER TABLE `megvasaroltgyogyszer`
  ADD CONSTRAINT `megvasaroltgyogyszer_ibfk_1` FOREIGN KEY (`gyogyszer_id`) REFERENCES `gyogyszer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `megvasaroltgyogyszer_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
