﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.ControlCreator
{
    class MYBackgroundWorker : BackgroundWorker
    {
        public bool isWorkDone = false;


        Action<object, DoWorkEventArgs> DoWorkAction;
        Action<object, ProgressChangedEventArgs> OverProcessAction;
        Action<object, RunWorkerCompletedEventArgs> processIsFinishedCallback;

        public MYBackgroundWorker(
               Action<object, DoWorkEventArgs> action = null,
               Action<object, ProgressChangedEventArgs> overProcess = null,
               Action<object, RunWorkerCompletedEventArgs> processIsFinishedCallback = null
            )
        {
            OverProcessAction = overProcess;
            this.processIsFinishedCallback = processIsFinishedCallback;

            DoWorkAction = action;


            isWorkDone = false;
            
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += Instance_DoWork;
            RunWorkerCompleted += Instance_RunWorkerCompleted;
            ProgressChanged += Instance_ProgressChanged;
            RunWorkerAsync();

        }

        private void Instance_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            OverProcessAction?.Invoke(sender, e);
        }

        private void Instance_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!IsBusy) CancelAsync();
            isWorkDone = true;

            Dispose();

            processIsFinishedCallback?.Invoke(sender,e);
        }

        private void Instance_DoWork(object sender, DoWorkEventArgs e)
        {
            DoWorkAction?.Invoke(sender, e);

        }
    }
}
