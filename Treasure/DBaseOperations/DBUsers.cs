﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.DBaseOperations
{
    public class DBUsers
    {
        public static async void GetAllUser(Action<ObservableCollection<EntityModell.User>> callback)
        {
            ObservableCollection<EntityModell.User> pon = new ObservableCollection<EntityModell.User>();
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {

                    pon = new ObservableCollection<EntityModell.User>(await connection.QueryAsync<EntityModell.User>("select * from users"));

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon);
        }
        public static async void AddUser(EntityModell.User user , Action<bool> callback = null)
        {
            int pon = 0;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@Name",user.Name);
                    dnm.Add("@Password", user.Password);

                    pon = await connection.ExecuteAsync("INSERT INTO `users` ( `Name`, `Password`) VALUES (@Name, @Password)",dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon==1);
        }
        public static async void DelUser(int userID, Action<bool> callback = null)
        {
            int pon = 0;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@ID", userID);
                    pon = await connection.ExecuteAsync("DELETE FROM `users` WHERE `users`.`ID` = @ID", dnm);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon == 1);
        }
        public static async void UpdUser(EntityModell.User user, Action<bool> callback = null)
        {
            int pon = 0;
            try
            {

                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                {
                    var dnm = new DynamicParameters();
                    dnm.Add("@ID", user.ID);
                    dnm.Add("@Name", user.Name);
                    dnm.Add("@Password", user.Password);
                    //UPDATE `users` SET `Name` = 'Brigéf', `Password` = 'Cióla' WHERE `users`.`ID` = 5
                    //UPDATE `users` SET `Password` = 'Ciólos' WHERE `users`.`ID` = 5
                    pon = await connection.ExecuteAsync("UPDATE `users` SET `Name` = @Name, `Password` = @Password WHERE `users`.`ID` = @ID", dnm);
                    // var pn = await connection.ExecuteAsync("UPDATE users SET Name = @Name , Password = @Password WHERE users.ID = @ID", dnm,commandType: CommandType.Text);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            callback(pon == 1);
        }
    }
}
