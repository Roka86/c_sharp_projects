﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.EntityModell
{
    public class Medicine
    {
        public int ID { get; set; }
        public string nev { get; set; }
        public string reszletek { get; set; }
        public string adagolas { get; set; }
        public string kep { get; set; }
}
}
