﻿using AngleSharp.Dom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.DocumentOperations
{
    class BrowsedMedicineQuickInfoCreator
    {
        public BrowsedMedicineQuickInfoCreator(IDocument document, Action<List<string>> GyogyszerLista)
        {
            GyogyszerLista(document.GetElementsByClassName("ps_product_name").ToList()
                .Distinct( new ProductNameEqualityComparer())
               .Select(a=>a.InnerHtml).ToList());
        }
    }
    class ProductNameEqualityComparer : IEqualityComparer<IElement>
    {
        public bool Equals(IElement b1, IElement b2)
        {
            if (b2 == null && b1 == null)
                return true;
            else if (b1 == null || b2 == null)
                return false;
            else if (b2 != null && b1 != null && b1.InnerHtml == b2.InnerHtml)
                return true;
            else if (b2 != null && b1 != null && b1.InnerHtml != b2.InnerHtml)
                return false;
            else return false;
        }

        public int GetHashCode(IElement bx)
        {
            int hCode = bx.InnerHtml.Length;
            return hCode.GetHashCode();
        }
    }
}
