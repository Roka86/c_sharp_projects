﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using Treasure.DocumentOperations;
using Treasure.EntityModell;
using WpfAnimatedGif;

namespace Treasure.ControlCreator
{
    public class UsersMedicineListBoxItem : ListBoxItem
    {
        public PatikaResult medicine;

        public System.Windows.Controls.Image gify { get; private set; }
        public System.Windows.Controls.Image ProductImage { get; private set; }
        public int Mennyiseg { get; set; }
        public DateTime Lejarat { get; set; }
        public TextBox Menny { get; private set; }
        public DatePicker Lejar { get; private set; }
        public ListBox AnchorControlOfThis { get; }
        public MainWindow Instance { get; }
        public string Nev { get; set; }

        BitmapImage bi;
        public StackPanel ExpirationDateAndPictureStackPanelContainer;
        public StackPanel DatestckPanel;
        public TextBlock WarningMessage;
        public Border WarningMessageBorder;
        public bool AddedToDatabase;
        public int Browsed { get; private set; }
        private TextBlock MedicineName;

        public UsersMedicineListBoxItem(PatikaResult medicine, ListBox AnchorControlOfThis,MainWindow instance, bool buttonsWanted = true)
        {
            AddedToDatabase = medicine.ID > 0? true:false;
            Browsed = AddedToDatabase?0: 1;
            this.medicine = medicine;
            this.AnchorControlOfThis = AnchorControlOfThis;
            Instance = instance;
            Nev = medicine.nev;
            Mennyiseg = medicine.mennyiseg;
            Lejarat = medicine.lejarat;
            Width = 770;
            Height = 210;
            BorderBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xad, 0x75, 0x99, 0xaf));//7599AF
            BorderThickness = new Thickness(3, 3, 3, 3);

            var stckPanel = new StackPanel()
            {
                Width = 805,
                Height = 208,
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };


            //Portion StacPanel
            var PortionstckPanel = new StackPanel()
            {
                Width = 80,
                Height = 208,
                Orientation = Orientation.Vertical,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };
            PortionstckPanel.Children.Add(new TextBlock()
            {
                FontSize = 16,
                Width = 80,
                Height = 40,
                Text = "Eredeti:"
            });
            PortionstckPanel.Children.Add(new TextBox()
            {
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                Text = medicine.adagolas,
                FontSize = 16,
                Width = 80,
                Height = 50
            });
            PortionstckPanel.Children.Add(new TextBlock()
            {
                FontSize = 16,
                Width = 80,
                Height = 20,
                Text = "Jelenlegi:"
            });
            Menny = new TextBox()
            {
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                //Text = medicine.ID > 0 ? $"{medicine.mennyiseg}" : medicine.adagolas,
                Text = $"{medicine.mennyiseg}" ,
                FontSize = 16,
                Width = 80,
                Height = 50
            };
            PortionstckPanel.Children.Add(Menny);


            stckPanel.Children.Add(PortionstckPanel);


            //2. Oszlop

            CheckExpirationAndQuantity(out bool IsDateExpire,out bool IsQuantityLessThanNeededForAWeek);
            var mostOuterStackPanel = new StackPanel()
            {
                Width = 380,
                Height =208,
                Orientation = Orientation.Vertical,
                VerticalAlignment = VerticalAlignment.Top
                //HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };
             ExpirationDateAndPictureStackPanelContainer = new StackPanel()
            {
                Width = 380,
                //Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 108:208,
                Orientation = Orientation.Horizontal,
                //HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };
             DatestckPanel = new StackPanel()
            {
                Width = 180,
                //Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 108:208,
                Orientation = Orientation.Vertical,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };
            DatestckPanel.Children.Add(new TextBlock()
            {
                FontSize = 16,
                Width = 180,
                Height = 20,
                Text = "Lejárati Dátum:"
            });
            Lejar = new DatePicker()
            {
                Width = 180,
                //Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 30:200,
                SelectedDate =  medicine.ID > 0?medicine.lejarat:DateTime.Now.AddMonths(2)
            };
            DatestckPanel.Children.Add(Lejar);

            // Image of Product
            //stckPanel.Children.Add(DatestckPanel);

            bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(medicine.image, UriKind.Absolute);
            bi.EndInit();
            ProductImage = new System.Windows.Controls.Image()
            {
                Width = 200,
                //Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 95:200,
                Source = bi
            };
            ExpirationDateAndPictureStackPanelContainer.Children.Add(DatestckPanel);
            ExpirationDateAndPictureStackPanelContainer.Children.Add(ProductImage);
            mostOuterStackPanel.Children.Add(ExpirationDateAndPictureStackPanelContainer);

            // Medicine expire or quantity of it is less than what need for one week
            
           
            //if (  IsDateExpire || IsQuantityLessThanNeededForAWeek )
            //{
                
                //var ExpiMessa = new StringBuilder();
                //ExpiMessa.AppendLine((IsDateExpire ? "The product is expired." : ""));
                //ExpiMessa.Append(IsQuantityLessThanNeededForAWeek ? "The product quantity is less than twelve percent." : "");
                mostOuterStackPanel.Children.Add(
                   WarningMessageBorder= new Border()
                   {
                       Width = 360,
                       //Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 0 : 100,//100
                       Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0x6d, 0x72, 0x2d, 0x35)),//722D35

                       Child =
                    WarningMessage = new TextBlock()
                    {
                        Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xad, 0xff, 0xf5, 0xaa)),//FFF5AA
                        VerticalAlignment = VerticalAlignment.Center,
                        FontFamily = new System.Windows.Media.FontFamily("Rockwell Extra Bold"),//Lucida Handwriting
                        FontSize = 15,
                        Width = 350,
                        Height = 95,
                        LineHeight = 20,
                        Padding = new Thickness(5.0, 5.0, 5.0, 5.0),
                        TextAlignment = TextAlignment.Center,
                        TextWrapping = TextWrapping.Wrap,
                        //Text = ExpiMessa.ToString()
                    }
                   }
                    );
            // }

            SetWarningMessageIfSo();

            stckPanel.Children.Add(mostOuterStackPanel);
            //stckPanel.Children.Add(ProductImage);
            if (buttonsWanted)
            {
                BitmapImage addButtSource = new BitmapImage();
                addButtSource.BeginInit();
                addButtSource.UriSource = new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\add.png"));
                addButtSource.EndInit();
                var buttOne = new Button()
                {
                    //Content = new TextBlock() { FontSize = 14, Text = "Add" }, Width = 50, Height = 50
                    Content = new System.Windows.Controls.Image() { Width = 50, Height = 50 ,Source= addButtSource }
                };
                //Add Button Click Event Gyógyszer adatbázishoz adás, és személyhez kötés
                buttOne.Click += (o, e) =>
                {
                    if (!AddedToDatabase)
                    {
                        var NwMdc = new Medicine()
                        {
                            kep = medicine.image,
                            adagolas = medicine.adagolas,
                            nev = medicine.nev,
                            reszletek = medicine.reszletek
                        };
                        DBaseOperations.DBMedicins.MedicineExists(NwMdc, (a, b) =>
                        {
                            if (a)
                            {
                                switch (MessageBox.Show("You have got another medicine with the same name.\n Do you want to save this medicine with a modified name ?"
                                    , caption: "Question", button: MessageBoxButton.YesNoCancel))
                                {
                                    case MessageBoxResult.OK:
                                    case MessageBoxResult.Yes: NwMdc.nev = b; break;
                                    case MessageBoxResult.No: break;
                                    case MessageBoxResult.Cancel: break;
                                    default: break;

                                }
                            }
                        });
                        DBaseOperations.DBMedicins.AddMedicine(
                            NwMdc, a =>
                        {
                        // Sikeresen hozzáadva az adatbázishoz a gyógyszer.
                        if (a)
                            {

                            //Adding Medicine to User
                            AddMedicineTUsr(NwMdc);

                            }
                            else
                            {
                                //AddMedicineTUsr(NwMdc);
                            //MessageBox.Show("Something went wrong.");
                        }
                        });
                    }
                };

                //var buttTwo = new Button() { Content = new TextBlock() { FontSize = 14, Text = "View" }, Width = 50, Height = 50 };
                BitmapImage ViewButtSource = new BitmapImage();
                ViewButtSource.BeginInit();
                ViewButtSource.UriSource = new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\viewDescription.png"));
                ViewButtSource.EndInit();
                var buttTwo = new Button()
                {
                    //Content = new TextBlock() { FontSize = 14, Text = "Add" }, Width = 50, Height = 50
                    Content = new System.Windows.Controls.Image() { Width = 50, Height = 50, Source = ViewButtSource }
                };

                buttTwo.Click += (o, e) => { new DescriptionWindow(this.medicine.reszletek).ShowDialog(); };

                BitmapImage updateButtSource = new BitmapImage();
                updateButtSource.BeginInit();
                updateButtSource.UriSource = new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\arrows.png"));
                updateButtSource.EndInit();
                var buttThree = new Button()
                {
                    //Content = new TextBlock() { FontSize = 14, Text = "Add" }, Width = 50, Height = 50
                    Content = new System.Windows.Controls.Image() { Width = 50, Height = 50, Source = updateButtSource }
                };
                //var buttThree = new Button() { Content = new TextBlock() { FontSize = 14, Text = "Update" }, Width = 50, Height = 50 };
                buttThree.Click += (o, e) =>
                {

                    GetIntNumberValueFromAString(Menny.Text,out int ujMennyiseg);

                    DBaseOperations.DBMedicins.UpdateUsersMedicineDetails(
                        new BoughtMedicine()
                        {
                            ID = medicine.ID,
                            gyogyszer_id = medicine.gyogyszer_id,
                            lejarat = Lejar.SelectedDate ?? DateTime.Now,
                            mennyiseg = ujMennyiseg,
                            user_id = medicine.user_id
                        },
                        IsUpdated => 
                        {
                            if (IsUpdated)
                            {
                                AnchorControlOfThis.Items.Remove(this);
                                Instance.OpenUserMedicines(this);
                                var dispMedNev = medicine.nev.Split(' ')?.First()??medicine.nev;
                                new DescriptionWindow($"{dispMedNev} is updated", 2250, 18).ShowDialog();
                            }
                        }
                    );
                };
                BitmapImage delButtSource = new BitmapImage();
                delButtSource.BeginInit();
                delButtSource.UriSource = new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\delete.png"));
                delButtSource.EndInit();
                var buttFour = new Button()
                {
                    //Content = new TextBlock() { FontSize = 14, Text = "Add" }, Width = 50, Height = 50
                    Content = new System.Windows.Controls.Image() { Width = 50, Height = 50, Source = delButtSource }
                };
                //var buttFour = new Button() { Content = new TextBlock() { FontSize = 14, Text = "Delete" }, Width = 50, Height = 50 };

                buttFour.Click += (o, e) =>
                {
                    DBaseOperations.DBMedicins.DeleteUsersMedicine(
                        medicine.ID,
                        IsDeleted =>
                        {
                            if (IsDeleted)
                            {
                                AnchorControlOfThis.Items.Remove(this);
                                var dispMedNev = medicine.nev.Split(' ')?.First() ?? medicine.nev;
                                new DescriptionWindow($"{dispMedNev} is deleted", 2050, 18).ShowDialog();
                            }
                        });
                };

                var stckTwo = new StackPanel() { Width = 55, Height = 200, Orientation = Orientation.Vertical };
                stckTwo.Children.Add(buttOne);
                stckTwo.Children.Add(buttTwo);
                stckTwo.Children.Add(buttThree);
                stckTwo.Children.Add(buttFour);
                stckPanel.Children.Add(stckTwo);
            }
            //<Image Name="LoaderGif" Opacity="0.6" Visibility="Hidden" Grid.Column="2" Grid.Row="1" gif:ImageBehavior.RepeatBehavior="Forever"
            //gif: ImageBehavior.AnimatedSource = "Resources/loader.gif" />

            var InfoStackPanel = new StackPanel()
            {
                Width = 140,
                Height = 208,
                Orientation = Orientation.Vertical,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };

            InfoStackPanel.Children.Add(MedicineName = new TextBlock()
            {
                FontSize = 14,
                Width = 139,
                Height = 140,
                LineHeight = 18,
                Padding = new Thickness(5.0, 5.0, 5.0, 5.0),
                  TextAlignment = TextAlignment.Center,
                  TextWrapping = TextWrapping.Wrap, 
                Text = medicine.nev
            });

            gify = new System.Windows.Controls.Image()
            {
                Width = 60,
                Height = 60,
                
            };

            BitmapImage bj = new BitmapImage();
            bj.BeginInit();
            bj.UriSource = (medicine.ID <= 0 ? new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\BrowsedMedicin.png"))
                            : new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\database.png") ));
            bj.EndInit();
            gify.Source = bj;

            InfoStackPanel.Children.Add(gify);

            stckPanel.Children.Add(InfoStackPanel);
            Content = stckPanel;
        }
        private void SetWarningMessageIfSo(bool wantDispatcher = false)
        {
            CheckExpirationAndQuantity(out bool IsDateExpire, out bool IsQuantityLessThanNeededForAWeek);
            var ExpiMessa = new StringBuilder();
            ExpiMessa.AppendLine((IsDateExpire ? "The product is expired." : ""));
            ExpiMessa.Append(IsQuantityLessThanNeededForAWeek ? "The product quantity is less than twelve percent." : "");

                ResizeElementsForWarningMessage(ExpiMessa,  IsDateExpire,  IsQuantityLessThanNeededForAWeek);
        }
        private void ResizeElementsForWarningMessage(StringBuilder ExpiMessa,bool IsDateExpire,bool IsQuantityLessThanNeededForAWeek)
        {
           
                WarningMessage.Text = ExpiMessa.ToString();
                //Warning message is written to textblock.

                WarningMessageBorder.Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 100 : 0;

                ProductImage.Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 95 : 200;

                Lejar.Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 30 : 200;
                DatestckPanel.Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 108 : 208;
                ExpirationDateAndPictureStackPanelContainer.Height = IsDateExpire || IsQuantityLessThanNeededForAWeek ? 108 : 208;
            
        }
        private void GetIntNumberValueFromAString(string v, out int ujNum)
        {
            var rgx = new Regex(@"[0-9]{1,8}", RegexOptions.IgnoreCase);
            var mtch = rgx.Match(v);


            int.TryParse(v.Substring(mtch.Index, mtch.Length), out int ujM);
            ujNum = ujM;
        }

        private void AddMedicineTUsr(Medicine NwMdc)
        {
            DBaseOperations.DBMedicins.GetMedicineIDAssociatedByItsName(NwMdc, id =>
            {
            if (id > 0)
            {
                Dispatcher.Invoke(() => {MedicineName.Text = NwMdc.nev; UpdateLayout(); });


                    GetIntNumberValueFromAString(Menny.Text, out int ujMennyisseg);
                    DBaseOperations.DBMedicins.AddMedicineToUser(
                        new BoughtMedicine()
                        {
                            gyogyszer_id = id,
                            user_id = medicine.CurrUserId,
                            lejarat = Lejar.SelectedDate.Value,
                            mennyiseg = ujMennyisseg
                        }
                        , IsMedicineAddedToUser =>
                        {
                            if (IsMedicineAddedToUser)
                            {
                                AddedToDatabase = true;
                                DBaseOperations.DBMedicins.GetIDFromUserAndMedicine(medicine.CurrUserId, id, megvettgyogyszerID => medicine.ID = megvettgyogyszerID);
                                //MessageBox.Show("Added");
                                var dispMedNev = medicine.nev.Split(' ')?.First() ?? medicine.nev;
                                new DescriptionWindow($"{dispMedNev} added", 2050, 18).ShowDialog();
                                BitmapImage bej = new BitmapImage();
                                bej.BeginInit();
                                bej.UriSource = new Uri(Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"Resources\database.png"));
                                bej.EndInit();

                                gify.Source = bej;
                                gify.UpdateLayout();
                            }
                        }

                        );
                }
                else
                {
                    new DescriptionWindow("Coudn't add medicine to database.", 2250, 18);
                }
            }); 
        }
        private  void CheckExpirationAndQuantity(out bool IsDateExpire, out bool IsQuantityLessThanNeededForAWeek)
        {
            IsDateExpire = new TimeSpan(medicine.lejarat.Ticks - DateTime.Now.Ticks).TotalDays <= 0 && medicine.ID > 0;
            GetIntNumberValueFromAString(medicine.adagolas, out int ujNum);
            IsQuantityLessThanNeededForAWeek = ((float)medicine.mennyiseg) / ((float)ujNum) < 0.125 && medicine.ID > 0;
        }
        private static byte[] GetMediumBlobFromImage(ImageSource imgSrc)
        {
            var bmp = imgSrc as BitmapImage;

            int height = bmp.PixelHeight;
            int width = bmp.PixelWidth;
            int stride = width * ((bmp.Format.BitsPerPixel + 7) / 8);

            byte[] bits = new byte[height * stride];
            bmp.CopyPixels(bits, stride, 0);

            return bits;
        }
    }
}
