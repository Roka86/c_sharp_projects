﻿namespace HTMLBindingByFox
{
    public static class URLAddresses
    {
        public enum MyUrls
        {
            None,
            Sztaki_Dictionary,
            Wikipedia_Hungarian_NameDays,
            InternetRadioDotCom_MusicGenres,
            PingvinPatika
        }
        public static readonly string[] Urls = new string[]{
            @"",
            @"http://szotar.sztaki.hu/search?fromlang=all&tolang=all&searchWord={0}&langcode=hu&u=0&langprefix=&searchMode=WORD_PREFIX&viewMode=simple&ignoreAccents=1",
            @"https://en.wikipedia.org/wiki/Name_days_in_Hungary",
            @"https://www.internet-radio.com/stations/",
            @"https://pingvinpatika.hu/catalogsearch/result/?q={0}"
        };
        public static string MyUrl(MyUrls mu)
        {
            return Urls[(int)mu];
        }
    }
}
