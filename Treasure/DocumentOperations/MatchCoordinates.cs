﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.DocumentOperations
{
    public class MatchCoordinates
    {
        public MatchCoordinates( int Start,int End)
        {
            this.Start = Start;
            this.End = End;
        }
        public int Start { get; set; }
        public int End { get; set; }
    }
}
