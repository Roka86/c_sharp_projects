﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.ControlCreator
{
    public class MyPoint
    {
        public MyPoint(double X,double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public double X { get; }
        public double Y { get; }
    }
}
