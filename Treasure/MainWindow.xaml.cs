﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Treasure.ControlCreator;
using Treasure.DocumentOperations;
using Treasure.EntityModell;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using ListBox = System.Windows.Controls.ListBox;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using TextBox = System.Windows.Controls.TextBox;

namespace Treasure
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Props

        
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return System.IO.Path.GetDirectoryName(path);
            }
        }
        public bool GotMSG = false;
        public Task SearchCloserAtTooMuchWaitting { get; private set; }
        public Task LeftMenuGrow { get; private set; }
        public Task LeftMenuShrink { get; private set; }
        public Task BrowserStart { get; private set; }
        public Task EmblemTask { get; private set; }
        public Task PharmacyTask { get; private set; }
        public Task UpdateIconTask { get; private set; }
        public EntityModell.User CurrentSelectedUser { get; set; }
        public bool IsCurrentUserLogedIn = false;
        private static CancellationTokenSource ShrinktokenSource ;
        private static CancellationToken Shrinktoken  ;
        private static CancellationTokenSource GrowtokenSource;
        private static CancellationToken Growtoken;
        private static CancellationTokenSource BrowserStarttokenSource;
        private static CancellationToken BrowserStarttoken;
        private static CancellationTokenSource EmblemTokenSource;
        private static CancellationToken EmblemToken;
        private static CancellationTokenSource PharmacyTokenSource;
        private static CancellationToken PharmacyToken;
        private ICollectionView myDataView;
        List<UsersMedicineListBoxItem> BrowsedItems;
        private MYBackgroundWorker SearchingDelay;

        #endregion
        #region ctor


        public MainWindow()
        {
            InitializeComponent();
            Height = Screen.PrimaryScreen.Bounds.Height-50.0;
            bool IsThere = false;
            DBaseOperations.DBChecking.CheckDataTablesExistance(AreDataTablesCreated=> 
            {
                if (AreDataTablesCreated)
                {
                    MessageBox.Show("'pharmacy' database exists, and the required datatables have been created.\n Run again the application!");
                    Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong when connecting to 'pharmacy' database.");Close();
                }
                IsThere = true;
            });
            if (!IsThere)
            {
                Welcome();
                EmblemMove();
            }
        }

        #endregion
        #region Animation
        //Lecsökkenti a bal menü szélességét 30 -ra, animálva;
        private void ShrinkLeftMenu()
        {
            if (LeftMenuGrow != null && !LeftMenuGrow.IsCompleted )
            {
                GrowtokenSource.Cancel(); ShrinktokenSource.Cancel();
            }
            else
            {
                ShrinktokenSource = new CancellationTokenSource();
                Shrinktoken = ShrinktokenSource.Token;

                LeftMenuShrink =
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(650);
                    double wd = 0.0;
                    Dispatcher.Invoke(() => { if (this.IsActive) wd = LeftMenu.Width; });
                    
                    while (wd > 30.0)
                    {

                    Dispatcher.Invoke(() => { if (this.IsActive) LeftMenu.Width = wd; });
                        Thread.Sleep(20); wd -= 20.0;
                    }
                    Dispatcher.Invoke(() => LeftMenu.Width = 45.0);
                }, Shrinktoken);
            }
        }
        

        
        //Megnöveli a bal menüt 160 szélességig, animálva.
        private void GrowLeftMenu()
        {
            if (LeftMenuShrink != null && !LeftMenuShrink.IsCompleted )
            {
                ShrinktokenSource?.Cancel();
                GrowtokenSource?.Cancel();
            }
            else
            {
                GrowtokenSource = new CancellationTokenSource();
                Growtoken = GrowtokenSource.Token;

                    LeftMenuGrow =
                Task.Factory.StartNew(() =>
                {
                    double wd = 0.0;
                    Dispatcher.Invoke(() => wd = LeftMenu.Width);
                    while (wd < 320.0)
                    {
                        wd += 45.0;
                        Dispatcher.Invoke(() => LeftMenu.Width = wd);
                        Thread.Sleep(25);
                    }

                },Growtoken);
            }
        }
        private void EmblemMove()
        {
            EmblemTokenSource = new CancellationTokenSource();
            EmblemToken = EmblemTokenSource.Token;

            EmblemTask =
            Task.Factory.StartNew(() =>
            {
                double StepQuantity,fll=1000;
                double wd = 0.0;
            bool IsMinus = true;
            Dispatcher.Invoke(() => wd = Emblem.Margin.Top);
            while (!EmblemToken.IsCancellationRequested)
            {
                if (wd < -450.0)
                {
                    IsMinus = false;
                }
                if (wd > 680.0)
                {
                    IsMinus = true;
                }
                    fll = IsMinus ? (fll - 1) : (fll + 1);
                StepQuantity =(wd!=0.0?1.0-(wd/1000):(IsMinus?(1.0):(-1.0)));
                     wd = (IsMinus?(wd-35.0*StepQuantity):(wd+ 35.0 * StepQuantity));

                    Dispatcher.Invoke(() => Emblem.Margin = new Thickness(-4, wd, 3, 0));
                    Thread.Sleep(565);
                }

            }, EmblemToken);
        }
        private void Welcome()
        {
            PharmacyTokenSource = new CancellationTokenSource();
            PharmacyToken = PharmacyTokenSource.Token;

            PharmacyTask =
            Task.Factory.StartNew(() =>
            {
            double wd = 0.0, op = 0.0;
            Dispatcher.Invoke(() => { wd = PharmacyLogo.Width; op = PharmacyLogo.Opacity; });
            while (!EmblemToken.IsCancellationRequested && wd < 800.0)
            {
                Dispatcher.Invoke(() => {PharmacyLogo.Width = wd; PharmacyLogo.Opacity = op; });
                    wd += 50.0;
                    op += op + 0.1 < 1.0 ? 0.1 : 0.0;
                    Thread.Sleep(6+(int)(40.0*op));
            }
                Thread.Sleep(1200);
                Dispatcher.Invoke(() => PharmacyLogo.Opacity=0.0);
            },PharmacyToken);
        }
        #endregion
        #region Controls_Events

        
        //Left menu mouse leave event
        private void notify_MouseLeave(object sender, MouseEventArgs e)
        {
            if (LeftMenu.Width >30.0)
            {
                if (LeftMenuGrow != null)
                {
                    Task.Factory.ContinueWhenAll(new Task[] { LeftMenuGrow }, a =>
                     {
                         ShrinkLeftMenu();
                     });
                }
                else
                {
                    ShrinkLeftMenu();
                }
            }
        }

        
        //Left menu captured event
        private void LeftMenu_GotMouseCapture(object sender, MouseEventArgs e)
        {
            if (LeftMenuShrink != null)
            {
                Task.Factory.ContinueWhenAll(new Task[] { LeftMenuShrink }, a =>
                {
                    GrowLeftMenu();
                });
            }
            else
            {
                GrowLeftMenu();
            }
        }
        //Users button Selected event
        private void ListBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            // Beállítja a megfelelő border méretét.
            UsersPage.Width = 1015.0; UsersMedicinesPage.Width = 0.0; MedicinesPage.Width = 0.0;

            if(CurrentSelectedUser == null || !IsCurrentUserLogedIn)new CreateUsersListBoxItems(this);
        }
        //Medicines Button click Event
        private void ListBoxItem_Selected_1(object sender, RoutedEventArgs e)
        {
            AllMedicineListBox.Items.Clear();
            DBaseOperations.DBMedicins.GetAllMedicine( allMed => allMed.ForEach(ech => AllMedicineListBox.Items.Add(
                new SimpleMedicineRow(new Medicine()
                {
                    nev = ech.nev,
                    reszletek = ech.reszletek,
                    adagolas = ech.adagolas,
                    kep = ech.kep
                })
                )) );
            UsersPage.Width = 0.0; UsersMedicinesPage.Width = 0.0; MedicinesPage.Width = 1015.0;
        }
        //User's Medicines Button click event
        private void ListBoxItem_Selected_2(object sender, RoutedEventArgs e)
        {

            OpenUserMedicines();
        }

        public void OpenUserMedicines(UsersMedicineListBoxItem aRow = null)
        {
            
            if (IsCurrentUserLogedIn)
            {
                UsersMedicineListBoxItem modifiedItem = null;
                if (DrugsList.Items.Count > 0)
                {
                    DrugsList.Items.Clear();

                    if (BrowsedItems != null)
                    {
                        foreach (var item in BrowsedItems)
                        {
                            if (aRow != null && item.medicine.ID != aRow.medicine.ID)
                            {
                                DrugsList.Items.Add(item);
                            }
                            else if (aRow != null && item.medicine.ID == aRow.medicine.ID)
                            {
                                modifiedItem = item;
                            }
                            else if (aRow == null)
                            {
                                DrugsList.Items.Add(item);
                            }

                        }
                        if (modifiedItem != null)
                        {
                            aRow = modifiedItem;
                        }
                    }
                }
                
                UsersMedicinesTitle.Text = $"{CurrentSelectedUser.Name}'s medicines";
                DBaseOperations.DBMedicins.GetUsersMedicines(CurrentSelectedUser.ID, medcns =>
                {
                    foreach (var item in medcns)
                    {
                        var elem = new UsersMedicineListBoxItem(new DocumentOperations.PatikaResult()
                        {
                            CurrUserId = CurrentSelectedUser.ID,
                            ID = item.ID,
                            nev = item.nev,
                            reszletek = item.reszletek,
                            adagolas = item.adagolas,
                            image = item.kep,
                            user_id = item.user_id,
                            gyogyszer_id = item.gyogyszer_id,
                            mennyiseg = item.mennyiseg,
                            lejarat = item.lejarat
                        }, DrugsList, this);

                        if(!DrugsList.Items.Contains(elem))DrugsList.Items.Add(elem);
                    }
                });
                
                UsersListSorting();

                if (aRow != null)DrugsList.ScrollIntoView(aRow);
                UsersPage.Width = 0.0; UsersMedicinesPage.Width = 1015.0; MedicinesPage.Width = 0.0;
                // DrugsList.ItemsSource
            }
            else
            {
                var dispMember = CurrentSelectedUser != null ? CurrentSelectedUser.Name : Environment.UserName;
                new DescriptionWindow($"Please log in {dispMember}, to access this function !", 1750, 18).ShowDialog();
            }
        }

        //Editor section password checking
        private void TBCurrPassw_KeyUp(object sender, KeyEventArgs e)
        {
            //if (CurrentSelectedUser != null && string.IsNullOrEmpty(CurrentSelectedUser.Password) || !IsCurrentUserLogedIn && CurrentSelectedUser != null && CurrentSelectedUser.Password != null && ((TextBox)sender).Text.Contains(CurrentSelectedUser.Password) )
            if (CurrentSelectedUser != null && TBCurrUser.Text.Equals(CurrentSelectedUser.Name) && !IsCurrentUserLogedIn &&  CurrentSelectedUser.Password != null && ((TextBox)sender).Text.Contains(CurrentSelectedUser.Password))
            {
                //EditorPopupInfo.Text = $"{CurrentSelectedUser.Name} is loged in.";
                new DescriptionWindow($"{CurrentSelectedUser.Name} is loged in.", 1750, 18).ShowDialog();
                IsCurrentUserLogedIn = true;
                TBCurrPassw.Text = CurrentSelectedUser.Password;
                if (DrugsList.Items.Count > 0) DrugsList.Items.Clear();

                foreach (var item in UsersGridTable.Items)
                {
                    var usr = ((UserModelListBoxItem)item);
                    if (usr.User.ID == CurrentSelectedUser.ID)
                    {
                        usr.IsLoggedIn = true;usr.UserName.Background = new SolidColorBrush(Color.FromArgb(0xad, 0x9f, 0xa8, 0x7a));
                    }
                    else
                    {
                        usr.IsLoggedIn = false; usr.UserName.Background = new SolidColorBrush(Color.FromArgb(0xad, 0x77, 0x63, 0x57));
                    }
                }

                
            }
        }
        
        //Selection  changed
        private void UsersGridTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CurrentSelectedUser = ((UserModelListBoxItem)(sender as ListBox).SelectedItem)?.User?? new EntityModell.User();
            IsCurrentUserLogedIn = string.IsNullOrEmpty(CurrentSelectedUser?.Password) ? true : false;
            TBCurrPassw.Text = string.IsNullOrEmpty(CurrentSelectedUser?.Password) ? "" : "*******************";
            EditorPopupInfo.Text = "";
            foreach (var item in UsersGridTable.Items)
            {
                ((UserModelListBoxItem)item).IsLoggedIn = false;
                ((UserModelListBoxItem)item).UserName.Background = new SolidColorBrush(Color.FromArgb(0xad, 0x77, 0x63, 0x57));
            }
        }
        
        //Adding New User
        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TBCurrUser.Text))
            {

                DBaseOperations.DBUsers.GetAllUser(users => AddUserNextStep(users));
            }

        }

        private void AddUserNextStep(ObservableCollection<EntityModell.User> users)
        {


            var firstCriteria = users.Where(a => a.Name.Equals(TBCurrUser.Text)).FirstOrDefault();
            if (firstCriteria == null && !TBCurrPassw.Text.Contains('*') && !string.IsNullOrEmpty(TBCurrPassw.Text))
            {
                DBaseOperations.DBUsers.AddUser(new EntityModell.User()
                {
                    Name = TBCurrUser.Text,
                    Password = TBCurrPassw.Text
                }, isAdded =>
                {
                    if (isAdded)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            Dispatcher.Invoke(() => new CreateUsersListBoxItems(this));
                        });
                        new DescriptionWindow("Added", 1750, 18).ShowDialog();
                    }
                });
            }
            else if (firstCriteria != null)
            {
                new DescriptionWindow($"Please write another User name, {Environment.UserName} !", 1750, 18).ShowDialog();
            }
            else new DescriptionWindow($"Please write password for you profile, {Environment.UserName} !", 1750, 18).ShowDialog();

        }

        //Delete User if loged in.
        private void DelCurrUser_Click(object sender, RoutedEventArgs e)
        {
            if (IsCurrentUserLogedIn)
            {
                TBCurrPassw.Text = "";
                DBaseOperations.DBUsers.DelUser(CurrentSelectedUser.ID, isDeleted => 
                {
                    if (isDeleted)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            Dispatcher.Invoke(() => new CreateUsersListBoxItems(this));
                        });
                        new DescriptionWindow("Deleted", 1750, 18).ShowDialog();
                    }
                    });
            }
            else
            {
                new DescriptionWindow($"Please type your password to access any function, {CurrentSelectedUser?.Name} !", 1750, 18).ShowDialog();
            }
        }
        
        // Update User if loged in.
        private void EditCurrentUser_Click(object sender, RoutedEventArgs e)
        {
            if (IsCurrentUserLogedIn)
            {
                DBaseOperations.DBUsers.UpdUser(new EntityModell.User() { ID = CurrentSelectedUser.ID, Name = TBCurrUser.Text, Password = TBCurrPassw.Text }, isUpdated =>
                      {
                          if (isUpdated)
                          {
                              Task.Factory.StartNew(() =>
                              {
                                  Dispatcher.Invoke(() => { new CreateUsersListBoxItems(this); UsersListSorting(); });
                              });
                              new DescriptionWindow("Updated", 1750, 18).ShowDialog();
                          }
                      });
            }
            else new DescriptionWindow($"Please type your password to access any function, {CurrentSelectedUser?.Name} !", 1750, 18).ShowDialog();
        }
        private void DisplayPoppMessage(string str)
        {
            Task.Factory.StartNew(() =>
            {
                Dispatcher.Invoke(() => EditorPopupInfo.Text = str);
                Thread.Sleep(3800);
                Dispatcher.Invoke(() => EditorPopupInfo.Text = "");
            });
        }

        #region User's medicines

        // Kereső mező billentyű felengedés esemény.
        private void BrowserText_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BrowseEnter();
            }
            if(((TextBox)sender).Text.Length > 3 && e.Key != Key.Space && e.Key != Key.Enter && e.Key != Key.Delete && e.Key != Key.Back
                 && e.Key != Key.Left && e.Key != Key.Right && e.Key != Key.Home && e.Key != Key.End && e.Key != Key.Select && e.Key != Key.RightShift && 
                 e.Key != Key.LeftCtrl && e.Key != Key.Insert)
            {
                if (SearchingDelay == null)
                {
                    QuickSearchForMedicineNamesTimer(((TextBox)sender));
                }
                else
                {
                    if(SearchingDelay.IsBusy)SearchingDelay.CancelAsync();
                }
            }
        }

        private void QuickSearchForMedicineNamesTimer(TextBox sender)
        {
            SearchingDelay = new MYBackgroundWorker(
                (o, ee) =>
                {
                    int i = 4;
                    while ( --i > 0)
                    {
                        if (ee.Cancel) i = 3;

                        Thread.Sleep(550);
                    }
                    if (i < 1) ee.Result = "DONE";
                    else ee.Result = "";
                },
                  processIsFinishedCallback: (ao, ae) =>
                  {
                      if (ae.Result.ToString().Equals("DONE"))
                      {
                          QuickSearchForMedicineNames(sender);
                          SearchingDelay = null;
                      }
                    }
                );

        }

        private void QuickSearchForMedicineNames(TextBox TxtBx)
        {
            new HTMLBindingByFox.HtmlPage().GetDocumentAngleSharp(
                ArrivedDocument =>
                {
                    new BrowsedMedicineQuickInfoCreator(ArrivedDocument, MedicinesList =>
                    {
                        Point pt = TxtBx.TranslatePoint(new Point(0, 0), this);
                        pt.X = pt.X + this.Left;
                        pt.Y = pt.Y + this.Top;
                        new CreateMedicinesQuickSearchWindow(TxtBx, MedicinesList,pt,this);
                    });
                }
                , HTMLBindingByFox.URLAddresses.MyUrls.PingvinPatika
                , word: TxtBx.Text);
        }

        private void BrowseRun()
        {
            BrowserText.IsReadOnly = true;
            var myPopupWnd = new MYBackgroundWorker((o, e) =>
            {
                Dispatcher.Invoke(() =>
                {
                    new HTMLBindingByFox.HtmlPage().GetDocumentAngleSharp(
                doc =>
                {

                    new DocumentOperations.BrowsedMedicineResultCreator(
                        doc, CurrentSelectedUser.ID
                        , MedicinesList =>
                        {
                           
                            e.Result = MedicinesList;
                        });
                }
                , HTMLBindingByFox.URLAddresses.MyUrls.PingvinPatika
                , word: string.Join(" ",BrowserText.Text.Split(' ').Take(2)));
                });
                while (e.Result == null) { }
            }, processIsFinishedCallback: (o, e) =>
              {
                Dispatcher.Invoke(() =>
                    {
                        LoaderGif.Visibility = Visibility.Hidden;
                        
                        BrowserText.IsReadOnly = false;
                        UpdateLayout();
                        List<UsersMedicineListBoxItem> utz = new List<UsersMedicineListBoxItem>();
                        foreach (var item in ((List<PatikaResult>)e.Result))
                        {
                            var etz = new UsersMedicineListBoxItem(item, DrugsList, this);
                            utz.Add(etz);
                            DrugsList.Items.Add(etz);
                        }
                        BrowsedItems = utz;
                        DrugsList.Items.Refresh();
                        UsersListSorting();
                    });
            });
        }

        #endregion

        
        // Search Button
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BrowseEnter();
        }
        #endregion
        public void BrowseEnter()
        {
            if (BrowserText.Text.Length > 3)
            {
                // 15 másodperc után leállítja a keresést
                SearchCloserAtTooMuchWaitting = Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(15000);
                    if (LoaderGif.IsVisible == true)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            if (BrowserStarttokenSource != null) BrowserStarttokenSource.Cancel();
                            LoaderGif.Visibility = Visibility.Hidden;
                            BrowserText.IsReadOnly = false;
                            new DescriptionWindow("Didn't found anything.", 1750, 18).ShowDialog();
                        });
                    }
                });

                LoaderGif.Visibility = Visibility.Visible;
                ////modify
                if (BrowserStart != null && !BrowserStart.IsCompleted)
                {
                    BrowserStarttokenSource.Cancel(); BrowserStarttokenSource.Cancel();
                    Task.Factory.ContinueWhenAll(new Task[] { BrowserStart }, a => Dispatcher.Invoke(() => BrowseRun()));

                }
                else
                {
                    BrowserStarttokenSource = new CancellationTokenSource();
                    BrowserStarttoken = BrowserStarttokenSource.Token;

                    BrowseRun();
                }
                //modify


            }
            else
            {
                var dispMember = CurrentSelectedUser != null ? CurrentSelectedUser.Name : Environment.UserName;
                new DescriptionWindow($"Please write minimum 4 characters for searching word,{dispMember} !", 1750, 18).ShowDialog();
            }
        }
        // await for Left menu Task to complet
        private async void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShrinktokenSource != null && GrowtokenSource != null && EmblemTask != null)
            {
                ShrinktokenSource.Cancel(); // CancellationTokenSource
                GrowtokenSource.Cancel();
                EmblemTokenSource.Cancel();
                if (!LeftMenuShrink.IsCompleted || !LeftMenuGrow.IsCompleted || !EmblemTask.IsCompleted || !PharmacyTask.IsCompleted)
                {
                    //this.Hide();
                    e.Cancel = true;
                    await LeftMenuShrink;
                    await LeftMenuGrow;
                    await EmblemTask;
                    await PharmacyTask;
                    
                    this.Close();
                }
            }
        }

        private void SortUsersDugsListBoxItems(object sender, RoutedEventArgs e)
        {
            UsersListSorting(sender);
        }

        private void UsersListSorting(object sender=null)
        {
            ToggleBetweenButtons(sender, SortByName, SortByNameDesc, out byte IsNameAsc);
            ToggleBetweenButtons(sender, SortByExpirationDate, SortByExpirationDateDesc, out byte IsExpiAsc);
            ToggleBetweenButtons(sender, SortByDosage, SortByDosageDesc, out byte IsDosageAsc);

            DrugsList.Items.Refresh();

             myDataView = CollectionViewSource.GetDefaultView(DrugsList.Items);

            using (myDataView.DeferRefresh()) // we use the DeferRefresh so that we refresh only once
            {

                myDataView.SortDescriptions.Clear();

                //AddedToDatabase
                myDataView.SortDescriptions.Add(new SortDescription("Browsed", WhichOrder(false)));

                if (IsNameAsc != 2)
                    myDataView.SortDescriptions.Add(new SortDescription("Nev", WhichOrder(IsNameAsc == 0)));
                if (IsExpiAsc != 2)
                    myDataView.SortDescriptions.Add(new SortDescription("Lejarat", WhichOrder(IsExpiAsc == 0)));
                if (IsDosageAsc != 2)
                    myDataView.SortDescriptions.Add(new SortDescription("Mennyiseg", WhichOrder(IsDosageAsc == 0)));
                
            }
        }

        private void ToggleBetweenButtons(object sender,ToggleButton firstButton, ToggleButton secondButton, out byte IsFirtsOn)
        {
            var sbn = ConvToTgBtn(sender, firstButton.Name);
            bool nameAsc = firstButton.IsChecked ?? false;
            bool nameDesc = secondButton.IsChecked ?? false;

            if (sbn && nameAsc && nameDesc)
            {
                secondButton.IsChecked = false;
                IsFirtsOn = 0;
            }
            else if (!sbn && nameAsc && nameDesc)
            {
                firstButton.IsChecked = false;
                IsFirtsOn = 1;
            }
            else
            {
                IsFirtsOn = nameAsc?((byte)0):(nameDesc? (byte)1: ((byte)2));
            }

        }

        private bool ConvToTgBtn(object sender,string ToggleBtnName)
        {
            return sender != null ? ((ToggleButton)sender).Name.Equals(ToggleBtnName) : false;
        }
        private ListSortDirection WhichOrder(bool tf)
        {
            return tf ? ListSortDirection.Ascending : ListSortDirection.Descending;
        }
    }
}
