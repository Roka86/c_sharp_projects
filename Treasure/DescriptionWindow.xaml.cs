﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Treasure.ControlCreator;

namespace Treasure
{
    /// <summary>
    /// Interaction logic for DesciptionWindow.xaml
    /// </summary>
    public partial class DescriptionWindow : Window
    {
        private readonly bool viewWhile;

        public DescriptionWindow(string txt,  int delayMs = -1, int textSize = 16, bool ViewWhile = false, List<string> QuickSearch = null,MyPoint point=null)
        {
            InitializeComponent();
            if (QuickSearch == null)
            {
                ListDescriptionPanel.Width = 0.0;
                ListDescriptionPanel.Height = 0.0;
                mainText.Text = txt;
                mainText.FontSize = (double)textSize;
                if (delayMs > 0 || ViewWhile)
                {
                    SizeToContent = SizeToContent.WidthAndHeight;
                    WindowStyle = WindowStyle.None;
                    mainText.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    mainText.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    mainText.TextAlignment = TextAlignment.Center;
                    mainText.VerticalAlignment = VerticalAlignment.Center;
                    mainText.Foreground = new SolidColorBrush(Color.FromArgb(0x70, 0xd3, 0xff, 0x26)); //D3FF26
                    mainText.Background = new SolidColorBrush(Color.FromArgb(0xff, 0x20, 0x28, 0x10)); //202810
                    if (!ViewWhile)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            Thread.Sleep(200 + delayMs);
                            Dispatcher.Invoke(() => Close());
                        });
                    }
                }
                else
                {
                    mainText.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                    mainText.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
                    //Width="910" Height="418" Margin="0,0,-7,0"
                    Width = Screen.PrimaryScreen.Bounds.Width * 0.75;
                    Height = Screen.PrimaryScreen.Bounds.Height * 0.88;
                    mainText.Width = Width-20;
                    mainText.Height = Height-20;
                    mainText.Margin = new Thickness(0, 0, -7, 0);
                    SizeToContent = SizeToContent.Width;
                }
                Height = Screen.PrimaryScreen.Bounds.Height - 50.0;
                viewWhile = ViewWhile;
            }
            else
            {
                Properties.Settings.Default.QuickSearchSelected = "False";
                if (point != null)
                {
                    WindowStartupLocation = WindowStartupLocation.Manual;
                    WindowStyle = WindowStyle.None;
                    Top = point.Y;
                    Left = point.X;
                    SizeToContent = SizeToContent.WidthAndHeight;
                }
                TextDescriptionPanel.Width = 0.0;
                TextDescriptionPanel.Height = 0.0;
                MedicinesList.ItemsSource = QuickSearch;
                MedicinesList.SelectionChanged += (o, e) =>
                {
                    Properties.Settings.Default.QuickSearchString = 
                    ((System.Windows.Controls.ListBox)o).SelectedItem.ToString();

                    Properties.Settings.Default.QuickSearchSelected = "True";

                    Close();
                };
            }
        }
        public void WindowClose() { if (viewWhile)Close(); }
    }
}
