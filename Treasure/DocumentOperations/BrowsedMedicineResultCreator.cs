﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp.Dom;
using AngleSharp.Parser.Html;
using Dapper;

namespace Treasure.DocumentOperations
{
    public class BrowsedMedicineResultCreator
    {
        List<PatikaResult> patikaResults = new List<PatikaResult>();
        public BrowsedMedicineResultCreator(IDocument document, int CurrUserID, Action<List<PatikaResult>> GyogyszerLista)
        {
            var gotProductsRow = document.GetElementsByClassName("products-row");
            if (gotProductsRow.Length > 0)
            {//ps_info_holder ;ps_type
                var gotProductshyperlink = gotProductsRow.Select(a => a.GetElementsByClassName("ps_info_holder").ToList())
                    .Aggregate((a, b) =>
                    {
                        var elements = new List<IElement>();
                        elements.AddRange(a);
                        elements.AddRange(b);
                        return elements;
                    });
                //IHtmlCollection<IElement> gotProductshyperlink 
                var gotProductSGrid = gotProductsRow.Select(a => a.GetElementsByClassName("products-grid-item").ToList())
                    .Aggregate((a, b) =>
                    {
                        var elements = new List<IElement>();
                        elements.AddRange(a);
                        elements.AddRange(b);
                        return elements;
                    });
                //var gotProductshyperlink = gotProductsRow[0].GetElementsByClassName("ps_info_holder");
                //var gotProductSGrid = gotProductsRow[0].GetElementsByClassName("products-grid-item");
                List<IElement> lst = new List<IElement>();
                var IMGtagsCollection = gotProductSGrid.ToList().Select(a => (a.GetElementsByTagName("img").ToList().First())).ToList();
                var DESCriptons = gotProductshyperlink.ToList().Select(b => b.GetElementsByTagName("a").ToList().First()).ToList();
                var PORtions = gotProductshyperlink.ToList().Select(b => b.GetElementsByTagName("h4").ToList().First()).ToList();

                var j = -1;
                foreach (AngleSharp.Dom.Html.IHtmlImageElement item in IMGtagsCollection)
                {
                    var str = (++j < PORtions.Count ? PORtions[j].InnerHtml.ToString().Trim() : "");


                    //var mtch = rgx.Match(str);

                    var ListOfMatchCoordinates = CreateMatchCoordinates(new Regex(@"[0-9]{1,5}[xgm/]", RegexOptions.Multiline), str);
                    //ListOfMatchCoordinates = ListOfMatchCoordinates.Skip(1).ToList();
                    int enem = 0;
                    if (ListOfMatchCoordinates.Count > 1) int.TryParse(str.Substring(ListOfMatchCoordinates[0].Start, ListOfMatchCoordinates[0].End - ListOfMatchCoordinates[0].Start), out enem);

                    var strr = (ListOfMatchCoordinates.Count < 1 ? ("0") : (ListOfMatchCoordinates.Count > 1 && enem == 1 ?
                            (str.Substring(ListOfMatchCoordinates[1].Start, ListOfMatchCoordinates[1].End - ListOfMatchCoordinates[1].Start))
                        : (str.Substring(ListOfMatchCoordinates[0].Start, ListOfMatchCoordinates[0].End - ListOfMatchCoordinates[0].Start))));

                    if (strr.Length - 1 > -1) strr = strr.Substring(0, strr.Length - 1);

                    int.TryParse(strr, out int num);


                    //str = CreateStringWhithoutRegexMatches(CreateMatchCoordinates(new Regex(@"[<][^<>]*[>]", RegexOptions.Multiline), str), str)
                    //   .Replace("&nbsp;", " ");

                    patikaResults.Add(new PatikaResult()
                    {
                        CurrUserId = CurrUserID,
                        nev = item.AlternativeText
                        , image = item.Source
                        , reszletek = (j < DESCriptons.Count ?
                                                                    GetDescription(DESCriptons[j].GetAttribute("href"))
                                                                : ""),
                        adagolas = (str.IndexOf(strr) + 5 < str.Length ? str.Substring(str.IndexOf(strr), 5) : strr)
                        , mennyiseg = num

                    });
                }
                GyogyszerLista(patikaResults);
            }


        }


        public string CreateStringWhithoutRegexMatches(List<MatchCoordinates> lista, string originalStr)
        {
            string result = "";
            if (lista.Count >= 1)
            {
                for (int i = 0; i < lista.Count; i++)
                {
                    if (lista[i].Start >= 0 && lista[i].Start < originalStr.Length && lista[i].End >= 0 && lista[i].End < originalStr.Length)
                    {
                        if (i == 0)
                        {
                            result = string.Concat(result, originalStr.Substring(0, lista[i].Start));
                        }
                        else if (i == lista.Count - 1 && i != 0)
                        {
                            result = string.Concat(result, originalStr.Substring(lista[i].End));
                        }
                        else
                        {
                            result = string.Concat(
                                result,
                                originalStr.Substring(lista[i].End,
                                                        lista[i + 1].Start - lista[i].End));
                        }

                    }
                }
                return result;
            }
            else return originalStr;
        }
        private List<MatchCoordinates> CreateMatchCoordinates(Regex rgx, string str)
        {
            var mtch = rgx.Matches(str);
            List<MatchCoordinates> lstMtch = new List<MatchCoordinates>();
            var mtchList = mtch.GetEnumerator();
            while (mtchList.MoveNext())
            {
                Match CrrMtch = mtchList.Current as Match;
                var StartCrrMtch = CrrMtch.Index;
                var EndCrrMtch = StartCrrMtch + CrrMtch.Length;

                var nwMbr = new MatchCoordinates(StartCrrMtch, EndCrrMtch);
                lstMtch.Add(nwMbr);
                lstMtch.Sort((a, b) => a.Start - b.Start);
            }

            return lstMtch;
        }

        private string GetDescription(string v)
        {
            string Description = null;
            WebClient wc = null;
            try
            {
                wc = new WebClient();
                wc.Encoding = Encoding.UTF8;
                var s = wc.DownloadString(v);

                // Create a new parser front-end (can be re-used)
                var parser = new HtmlParser();
                //Just get the DOM representation
                var document = parser.Parse(s);
                var de = document.GetElementById("betegtaj")
                                                            .Children
                                                            .ToList();
                var desc = (de != null && de.Count > 0 ?
                                                            de.Select(elm => elm.InnerHtml)
                                                            .Aggregate((a, b) => a + b)
                                                        :
                        document.GetElementsByTagName("div")
                        .Where(a => a != null && a.HasAttribute("itemprop") && a.GetAttribute("itemprop").Equals("description")).Select(b => b).First()?.InnerHtml ?? "");

                desc = CreateStringWhithoutRegexMatches(CreateMatchCoordinates(new Regex(@"[<][^<>]*[>]", RegexOptions.Multiline), desc), desc)
                       .Replace("&nbsp;", " ");
                //if (string.IsNullOrEmpty(desc))
                //{

                // }

                desc = desc.Substring(0, (desc.Length > 7999 ? 7999 : desc.Length));
                Description = "";
                int i = 0;
                while (i < desc.Length)
                {
                    Description = string.Concat(Description, desc[i].ToString());
                    if (i % 150 == 0 && i != 0) Description = string.Concat(Description, "\n");
                    i++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {


                if (wc != null) wc.Dispose();
            }
            return Description;
        }
        private string ClearUnwantedStringsFromAnother(string patt, params string[] str)
        {
            string restr = patt;
            foreach (var item in str)
            {
                restr = restr.Replace(item, "").ToString();
            }
            return restr;
        }
        private Bitmap DownloadImageAsBitmapFromUrl(string ImageUrl)
        {
            Bitmap bmp;
            Stream s = null;
            WebClient wc = null;

            try
            {
                wc = new WebClient();
                s = wc.OpenRead(ImageUrl);
                //bmp = new Bitmap(s);
                bmp = new Bitmap(Image.FromStream(s), new Size(200, 200));
                //using (Bitmap bmp = new Bitmap(s))
                //{
                //    //bmp.Save(DownloadedFile, myImageCodecInfo, myEncoderParameters);
                //}
            }
            catch (Exception)
            {
                bmp = null;

            }
            finally
            {
                if (s != null) s.Dispose();

                if (wc != null) wc.Dispose();
            }
            return bmp;
        }
    }
}
