﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treasure.EntityModell
{
    public class BoughtMedicine
    {
        public int ID { get; set; }
        public int user_id { get; set; }
        public int gyogyszer_id { get; set; }
        public int mennyiseg { get; set; }
        public DateTime lejarat { get; set; }
    }
}
