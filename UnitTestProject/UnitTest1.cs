﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Treasure;
using Treasure.DocumentOperations;
using System.Linq;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// test ignore tags from a string
        /// </summary>
        [TestMethod]
        public void Test()
        {
            var str = File.ReadAllText(
                Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"TextFile.txt"));

            var st =CreateStringWhithoutRegexMatches(CreateMatchCoordinates(new Regex(@"[<][^<>]*[>]", RegexOptions.Multiline), str), str);
            Assert.IsTrue( !st.Contains(">") && !st.Contains("<"));
        }
        /// <summary>
        /// test numbers found in string regex
        /// </summary>
        [TestMethod]
        public void TestTwo()
        {
            var str = File.ReadAllText(
                Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"TextFile.txt"));

            var akarmi = CreateStringWhithoutRegexMatches(CreateMatchCoordinates(new Regex(@"[^0-9]*", RegexOptions.Multiline), str), str);

            int.TryParse(akarmi,out int result);

            Assert.IsTrue(
                result > 0
            );
        }

        public string CreateStringWhithoutRegexMatches(List<MatchCoordinates> lista, string originalStr)
        {
            string result = "";
            if (lista.Count >= 1)
            {
                for (int i = 0; i < lista.Count; i++)
                {
                    if (lista[i].Start >= 0 && lista[i].Start < originalStr.Length && lista[i].End >= 0 && lista[i].End < originalStr.Length)
                    {
                        if (i == 0)
                        {
                            result = string.Concat(result, originalStr.Substring(0, lista[i].Start));
                        }
                        else if (i == lista.Count - 1 && i != 0)
                        {
                            result = string.Concat(result, originalStr.Substring(lista[i].End));
                        }
                        else
                        {
                            result = string.Concat(
                                result,
                                originalStr.Substring(  lista[i].End,
                                                        lista[i + 1].Start - lista[i].End));
                        }

                    }
                }
                return result;
            }
            else return originalStr;
        }
        private List<MatchCoordinates> CreateMatchCoordinates(Regex rgx, string str)
        {
            var mtch = rgx.Matches(str);
            List<MatchCoordinates> lstMtch = new List<MatchCoordinates>();
            var mtchList = mtch.GetEnumerator();
            while (mtchList.MoveNext())
            {
                Match CrrMtch = mtchList.Current as Match;
                var StartCrrMtch = CrrMtch.Index;
                var EndCrrMtch = StartCrrMtch + CrrMtch.Length;

                var nwMbr = new MatchCoordinates(StartCrrMtch, EndCrrMtch);
                lstMtch.Add(nwMbr);
                lstMtch.Sort((a, b) => a.Start - b.Start);
 

            }
            
            return lstMtch;
        }
    }
}
