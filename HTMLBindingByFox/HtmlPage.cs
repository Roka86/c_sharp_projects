﻿using AngleSharp;
using AngleSharp.Dom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HTMLBindingByFox.URLAddresses;

namespace HTMLBindingByFox
{
    public class HtmlPage
    {
        public string[,] CurrenciesTable;
        public List<Regex> Mrgx { get; private set; }
        public double TheCurrencyChangedNumber { get; private set; }
        public double TheConvertableNumber { get; private set; }

        
        private delegate bool DoublePredicate<in T>(T a,T b);
        public static bool arrived = false;
        
        public List<string> otherLanguage;
        public List<string> hungarianLanguage;


        public HtmlPage()
        {
            
        }

        /// <summary>
        /// Query a web page, and creat an AngelSharp IDocument. \n We define in an Action what to do, if it arrives.
        /// </summary>
        /// <param name="gotDocument"></param>
        /// <param name="myUrls"></param>
        /// <param name="word"></param>
        public async void GetDocumentAngleSharp(
              Action<IDocument> gotDocument
            , MyUrls myUrls = MyUrls.None
            , string customAddress = ""
            , string word = "")
        {
            // Setup the configuration to support document loading
            var config = Configuration.Default.WithDefaultLoader();
            string address = string.Empty;
            // Load the names of all The Big Bang Theory episodes from Wikipedia
            if ( myUrls == MyUrls.None )
            {
                address = string.Format(customAddress, word);
            }else address = string.Format(MyUrl(myUrls), word);
            // Asynchronously get the document in a new context using the configuration
            gotDocument( await BrowsingContext.New(config).OpenAsync(address));

        }
 
        //If str contains any of the given strings then method returns false.
        private bool containsAnyStringArrayElements(string str,DoublePredicate<string> pred, params string[] prm)
        {
            for (int i = 0; i < prm.Length; i++)
            {
                if (pred.Invoke(str,prm[i]))
                {
                    return false;
                }
            }
            return true;
        }
        private bool equalsInnerStrings(List<string> list, string element)
        {
            var elem = element.ToLower();
            foreach (var item in list)
            {
                if (item.ToLower().Equals(elem))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
