﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Treasure.DBaseOperations
{
    public class DBChecking
    {
        // Ellenőrzi, hogy léteznek e az adattáblák, és létrehozza  őket, ha nem.
        public static async void CheckDataTablesExistance(Action<bool> popupWhnCreated)
        {
            try
            {
                IEnumerable<string> pon;
                using (IDbConnection connection = new MySqlConnection(Properties.Settings.Default.ConnStrRoot))
                {

                    pon = connection.Query<string>("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'pharmacy';");
                    if (pon.Count() == 0)
                    {
                        var usr = connection.Query<string>("select `User` from `mysql`.`user` where `Host` = 'localhost' and `User` = 'medicine'");
                        if (usr.Count() == 1)
                        {
                            connection.Execute(@"create database `pharmacy`;drop user 'medicine'@'localhost';CREATE USER 'medicine'@'localhost' IDENTIFIED BY 'great';
GRANT ALL PRIVILEGES ON `pharmacy`.* TO 'medicine'@'localhost' WITH GRANT OPTION;");
                        }
                        else
                        {
                            connection.Execute(@"create database `pharmacy`;CREATE USER 'medicine'@'localhost' IDENTIFIED BY 'great';
GRANT ALL PRIVILEGES ON `pharmacy`.* TO 'medicine'@'localhost' WITH GRANT OPTION;");
                        }

                    }
                    //select * from `mysql`.`user` where `Host` = 'localhost' and `User` = 'medicine'

                }
                Thread.Sleep(1000);
                if (pon.Count() == 0)
                {
                    using (IDbConnection conn = new MySqlConnection(Properties.Settings.Default.DBaseConnStr))
                    {
                        await conn.ExecuteAsync(File.ReadAllText(
                            Path.Combine(Path.Combine(MainWindow.AssemblyDirectory, @"..\.."), @"SQLScripts\PharmacySchemaCreation.sql")
                            ));
                        popupWhnCreated(true);
                    }
                }
            
            }
            catch (Exception ex)
            {
                popupWhnCreated(false);
                Console.WriteLine(ex.ToString());
            }

        }
    }
}
